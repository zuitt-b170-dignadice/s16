/* 
Repition Control Structures
    Loops - executes the codes repeatedly in a preset number of times or forever
*/

//  while loop - takes in an expression/condition before proceeding in the evaluation of the codes.

/* 
while (condition){
    statement/s
}
*/

let count = 5;

while (count !== 0){
    console.log('While loop: ' + count)

count -- // the count will continue without declaring the limit
};

/* 

 5 4 3 2 1 0

*/

let countTwo = 1;

while (countTwo < 11){
    console.log(countTwo);

    countTwo ++;
}

/* 

count from 1-10

*/

// do-while loop (safer version)- at least 1 codeblock to be executed before proceeding to the condition.

/* 

do{
    statement/s
}while(condition)

*/
let countThree = 5;

do{
    console.log('Do-While Loop: ' + countThree);

    countThree -- ;
} while (countThree > 0);

/* 

5 4 3 2 1 

*/

// -- reverse increment
// ++ forward decrement

let countFour = 1;

do{
    console.log(countFour);
    countFour++;
}while (countFour < 11)


// for loop - more flexible looping

/* 

    Syntax : 
        for(init; condition; finalExpression (++, --){
            statement/s;
        }

    Process 
        1. init - recognizing the variable created.
        2. condition - reading the condition
        3. Statement/s - executing th ecommands/statements.
        4. finalExpression - executing the final Expression

*/

for (let countFive = 5; countFive > 0; countFive--){
    console.log('For loop: ' + countFive)
}


let number = Number(prompt("Give me a Number"));
for (let numCount = 1; numCount <= number; numCount++){
	console.log("Hello Batch 170!")
}

let myName = 'alex';
console.log(myName.length); // counts the characters.
console.log(myName[2]); // index e

/* 

    .length - representing the number of characters inside the string; the charactes are all that are inclded in the string (spaces, symobls, punctuations)

    variable[X] - accessing the array of the variable; 0 based - counting begins at 0

*/

for (let x = 0; x < myName.length; x++){
    console.log(myName[x])
};

myName = 'Alex';
for (let i = 0; i < myName.length; i++){
    // toLowerCase() - converts every cahracter into lower case, regardless if they are originally uppercase.
    if (
        myName[i].toLowerCase() == 'a' || 
        myName[i].toLowerCase() == 'e' || 
        myName[i].toLowerCase() == 'i' || 
        myName[i].toLowerCase() == 'o' || 
        myName[i].toLowerCase() == 'u' 

    ){
        console.log(3)
    }else{
        console.log(myName[i]);
    }
}


// Continue and Break statements

    for (let countSix = 0; countSix <= 20; countSix++){
        if(countSix % 2 === 0) {
            continue; // tells the browser/codes to continue to the next ireation of the loop.
        }
        console.log('continue and break:' + countSix);
        if (countSix>10){
            break; // tells the browser/codes to terminate the loop even the condition in the for loop is met/satisfied
        }
    }

let x_name = 'Alexandro';
// name has 8 indeces but 9 elements
// indeces - from 0 
// element - from 1 
/* 

    compute for index : length -1
    compute for length : 

*/


for (let i = 0; i < x_name.length; i++){
    console.log(x_name[i]);

    if (x_name[i].toLowerCase() === 'a'){
        console.log('continue to the next iteration');
        continue;
    }

    if(x_name[i].toLowerCase() === 'd'){
        break;
    }
}